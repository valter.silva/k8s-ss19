.ONESHELL:

SHELL = /bin/sh

HEADER = "*****"

CLUSTER_NAME:=sql19
DEPLOYMENT=
REPLICASET=
SERVICE=
SQL_MASTER_IP=csl-svr-hdp02.ad.capricorn.com.au
KNOX_IP=10.3.205.74
#KNOX_PASSWORD=
POD=

.PHONY: check-cluster-clock
check-cluster-clock: ## Checks the clock on all hosts
	pssh -i -h ~/pssh/hosts timedatectl

.PHONY: create-cluster
create-cluster: ## Deploy SQL Server big data cluster on your Kubernetes cluster
	# Deploying a cluster can take hours, and SSH session might be closed.
	# Therefore running command with `nohup` keeps the command alive and running
	nohup mssqlctl create cluster $(CLUSTER_NAME) > deploy-cluster.log &
	# tail -f deploy-cluster.log

.PHONY: create-rook-operator
create-rook-operator: ## Creates Rook operator
	helm install --namespace rook-ceph-system --name rook-ceph rook-stable/rook-ceph -f rook-operator.yaml

# It's important to make sure that `dataDirHostPath` defined at rook-cluster.yaml
# has the same path and it's empty in all hosts,
.PHONY: create-rook-cluster
create-rook-cluster: ## Crete the Rook cluster
	pssh -i -h ~/pssh/hosts sudo rm -rf /data/3/sql19/
	pssh -i -h ~/pssh/hosts sudo mkdir -p /data/3/sql19/
	kubectl create -f rook-cluster.yaml

.PHONY: create-rook-dashboard
create-rook-dashboard: ## Crete the Rook dashboard
	kubectl create -f rook-dashboard.yaml

.PHONY: create-rook-storage-class
create-rook-storage-class: ## Crete the Rook dashboard
	kubectl create -f rook-storageclass.yaml

.PHONY: create-kubernetes-configuration
create-kubernetes-configuration:  ## Create Kubernetes configuration for the current user
	mkdir -p $$HOME/.kube
	sudo cp -i /etc/kubernetes/admin.conf $$HOME/.kube/config
	sudo chown $(shell id -u):$(shell id -g) $$HOME/.kube/config

.PHONY: delete-helm-rook-ceph-release
delete-helm-rook-ceph-release:
	helm del --purge rook-ceph

.PHONY: delete-cluster
delete-cluster: ## Delete SQL Server big data cluster on your Kubernetes cluster
	mssqlctl delete cluster $(CLUSTER_NAME)

.PHONY: delete-rook-storage-class
delete-rook-storage-class: ## Crete the Rook dashboard
	kubectl delete -f rook-storageclass.yaml

.PHONY: delete-rook-cluster
delete-rook-cluster: ## Deletes the Rook cluster
	kubectl delete -f rook-cluster.yaml

.PHONY: describe-service
describe-service: ## Describe one service only
	kubectl describe services $(SERVICE) -n $(CLUSTER_NAME)

.PHONY: describe-pod
describe-pod: ## Describe an individual pod for more details
	kubectl describe pod mssql-storage-pool-default-0 -n $(CLUSTER_NAME)

.PHONY: describe-deployment
describe-deployment: ## Describes only one deployment
	kubectl describe deployments $(DEPLOYMENT) -n $(CLUSTER_NAME)

.PHONY: describe-all-deployments
describe-all-deployments:  ## Describes all the deployment in Kubernetes
	kubectl describe deployments -n $(CLUSTER_NAME)

.PHONY: describe-replicaset
describe-replicaset: ## Describes only one replicaset
	kubectl describe replicasets $(REPLICASET) -n $(CLUSTER_NAME)

.PHONY: delete-service
delete-service: ## Exposes deployment as load balancer
	kubectl delete service $(SERVICE) -n $(CLUSTER_NAME)

.PHONY: expose-deployment
expose-deployment: ## Exposes deployment as load balancer
	kubectl expose deployment $(DEPLOYMENT) --type=NodePort --name=$(DEPLOYMENT)-lb -n $(CLUSTER_NAME)

.PHONY: get-k8-cluster-status
get-k8-cluster-status: ## Retrieve Kubernetes' cluster status
	kubectl get nodes

.PHONY: get-pods
get-pods: ## Inspect the status of the pods in your cluster.
	kubectl get pods -n $(CLUSTER_NAME)

.PHONY: get-pods-all-namespaces
get-pods-all-namespaces:  ## Get all pods across all namespaces
	kubectl get pods --all-namespaces

.PHONY: get-helm-rook-status
get-helm-rook-status: ## Gets Rooks status via HELM
	helm ls --all rook-ceph

.PHONY: get-all-replicasets
get-all-replicasets: ## Gets all replica sets
	kubectl get replicasets -n $(CLUSTER_NAME)

.PHONY: get-pod-logs
get-pod-logs: ## Gets the logs for one pod
	kubectl logs $(POD) --all-containers=true -n $(CLUSTER_NAME)

.PHONY: get-services
get-services: ## Review the cluster services during and after a deployment
	kubectl get svc -n $(CLUSTER_NAME)

.PHONY: get-deployments
get-deployments: ## Show deployments list
	kubectl get deployments -n $(CLUSTER_NAME)

.PHONY: get-database-external-ip
get-database-external-ip: ## Gets the database external IP within the cluster
	kubectl get svc endpoint-master-pool -n $(CLUSTER_NAME)
	kubectl get svc service-security-lb -n $(CLUSTER_NAME)
	kubectl get svc service-proxy-lb -n $(CLUSTER_NAME)

.PHONY: get-rook-operator-pods
get-rook-operator-pods: ## Gets Rook operator pods
	$(info $(HEADER) Check Rook operator status $(HEADER))
	@kubectl --namespace rook-ceph-system get pods -l "app=rook-ceph-operator"
	@kubectl -n rook-ceph-system get pod

.PHONY: get-rook-services
get-rook-services: ## Gets Rook services
	$(info Get Rook services)
	kubectl -n rook-ceph get service

.PHONY: get-rook-pods
get-rook-pods: ## Gets Rook pods
	$(info List pods in the rook namespace)
	kubectl -n rook-ceph get pod

.PHONY: get-rook-status
get-rook-status: get-rook-operator-pods get-rook-services get-rook-pods ## Gets Rooks related status

.PHONY: get-service
get-service: ## Get information about one service only
	kubectl get services $(SERVICE) -n $(CLUSTER_NAME)

.PHONY: load-sample-data
load-sample-data: ## Loads sample data into SQL Server
	./bootstrap-db.sh $(CLUSTER_NAME) $(SQL_MASTER_IP) $(MSSQL_SA_PASSWORD) $(KNOX_IP) $(KNOX_PASSWORD)

.PHONY: ping-servers
ping-servers: ## Reboots all servers
	ping -c 5 csl-svr-hdp05
	ping -c 5 csl-svr-hdp04
	ping -c 5 csl-svr-hdp03
	ping -c 5 csl-svr-hdp02

.PHONY: restart-ntp
restart-ntp: ## Forces update/synchornization of clock with NTP servers
	pssh -i -h ~/pssh/hosts sudo service ntpd stop
	sleep 5
	pssh -i -h ~/pssh/hosts sudo /usr/sbin/ntpd -gq
	sleep 5
	pssh -i -h ~/pssh/hosts sudo service ntpd start

.PHONY: restart-services
restart-services: stop-old-services ## Restart all services locally
	@echo "Disable SWAP in all servers"
	pssh -i -h ~/pssh/hosts sudo swapoff -a
	@echo "Restart Docker service"
	pssh -i -h ~/pssh/hosts sudo service docker restart
	@echo "Restart Kubelet service"
	pssh -i -h ~/pssh/hosts sudo service kubelet restart

.PHONY: reboot-servers
reboot-servers: ## Reboots all servers
	pssh -i -H csl-svr-hdp05 sudo reboot
	pssh -i -H csl-svr-hdp04 sudo reboot
	pssh -i -H csl-svr-hdp03 sudo reboot
	pssh -i -H csl-svr-hdp02 sudo reboot
	pssh -i -H csl-svr-hdp01 sudo reboot

.PHONY: retrieve-pods-log
retrieve-pods-log: ## Retrieve the logs for containers running in a pod
	kubectl logs mssql-storage-pool-default-0 --all-containers=true -n $(CLUSTER_NAME) > pod-logs.txt

.PHONY: restore-cap-db
restore-cap-db: ## Loads sample data into SQL Server
	./restore-cap-db.sh $(CLUSTER_NAME) $(SQL_MASTER_IP) $(MSSQL_SA_PASSWORD) $(KNOX_IP) $(KNOX_PASSWORD)

.PHONY: stop-old-services
stop-old-services: ## Stops all 'old' service, e.g.:MapR, Dremio, Cloudera
	-pssh -i -h ~/pssh/hosts sudo service cloudera-scm-agent stop
	-pssh -i -h ~/pssh/hosts sudo service cloudera-scm-server-db stop
	-pssh -i -h ~/pssh/hosts sudo service cloudera-scm-server stop
	-pssh -i -h ~/pssh/hosts sudo service dremio stop

.PHONY: status-services
status-services: ## Display services status (locally)
	pssh -i -h ~/pssh/hosts sudo service docker status
	pssh -i -h ~/pssh/hosts sudo service kubelet status

.PHONY: set-ntp-configuration
set-ntp-configuration:  ## Setup NTP configuration across all servers
	pscp.pssh -h ~/pssh/hosts -v set-ntp.sh /tmp/
	pssh -i -h ~/pssh/hosts chmod +x /tmp/set-ntp.sh
	pssh -i -h ~/pssh/hosts /bin/bash /tmp/set-ntp.sh
	pssh -i -h ~/pssh/hosts sudo service ntpd restart
	pssh -i -h ~/pssh/hosts date

# https://docs.microsoft.com/en-us/sql/big-data-cluster/deployment-guidance?view=sqlallproducts-allversions#upgrade
.PHONY: upgrade-to-a-new-release
upgrade-to-a-new-release: delete-cluster ## Currently, the only way to upgrade a big data cluster to a new release is to manually remove and recreate the cluster
	sudo pip3 install --extra-index-url https://private-repo.microsoft.com/python/ctp-2.2 mssqlctl

.PHONY: view-kubernetes-configuration
view-kubernetes-configuration: ## View the cluster configuration
	kubectl config view

help: ## Show all commands available and their description
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
