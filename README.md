## How to restore sample database

```
CLUSTER_NAMESPACE=sql19 SQL_MASTER_IP=<get-me-on-portal-master-instance> SQL_MASTER_SA_PASSWORD=<replace-me> KNOX_IP=<get-me-on-portal-endpoints> KNOX_PASSWORD=<knox-password>
```


## NTP

Remeber to install and configure NTP properly:

```
make set-ntp-configuration
``` 

## Storage

### Deploy the Rook Operator

### Create a ServiceAccount for Tiller in the `kube-system` namespace
```
kubectl --namespace kube-system create sa tiller
```

### Create a ClusterRoleBinding for Tiller
```
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
```

### Patch Tiller's Deployment to use the new ServiceAccount
```
kubectl --namespace kube-system patch deploy/tiller-deploy -p '{"spec": {"template": {"spec": {"serviceAccountName": "tiller"}}}}'
```

The Ceph Operator helm chart will install the basic components necessary to create a storage platform for your Kubernetes cluster. After the helm chart is installed, you will need to create a Rook cluster.
```
helm repo add rook-stable https://charts.rook.io/stable
helm install --namespace rook-ceph-system rook-stable/rook-ceph
```

To uninstall/delete the rook-ceph deployment:
```
helm delete --purge rook-ceph
```

### Delete any previous deployment
```
make delete-helm-rook-ceph-release
```

### Install Rook opearator using helm
```
make create-rook-operator
```

### Show status of Rook operator running on K8s cluster
```
make get-helm-rook-cepth-release-status
```

### Create Rook cluster
```
make create-rook-cluster
```

### Create Rook dashboard
```
make create-rook-dashboard

# Check its status:
kubectl -n rook-ceph get service

# Look for this line:
rook-ceph-mgr-dashboard-external-https   NodePort    10.107.133.178   <none>        8443:31822/TCP   5m49s
```

Now access the dashboard at `https://<kubernetes master node ip>:31822/#/dashboard`

#### Get dashboard credentias
```
    kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o yaml | grep "password:" | awk '{print $2}' | base64 --decode
```
