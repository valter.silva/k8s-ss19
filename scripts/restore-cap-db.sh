#!/usr/bin/env bash

CLUSTER_NAMESPACE=$1
SQL_MASTER_IP=$2
SQL_MASTER_SA_PASSWORD=$3
KNOX_IP=$4
KNOX_PASSWORD=$5

BACKUP_FILE=/var/opt/mssql/backup/DataWarehouse_DPP_Optimised_20181228.bak

# If Knox password is not supplied then default to SQL Master password
KNOX_PASSWORD=${KNOX_PASSWORD:=${SQL_MASTER_SA_PASSWORD}}

SQL_MASTER_INSTANCE=${SQL_MASTER_IP},31433
KNOX_ENDPOINT=${KNOX_IP}:30443

set -x

if [[ ! -f "${BACKUP_FILE}" ]]; then
    echo " BACKUP FILE NOT FOUND "
fi

kubectl cp "${BACKUP_FILE}" mssql-master-pool-0:/var/opt/mssql/data -c mssql-server -n ${CLUSTER_NAMESPACE}
sqlcmd -S ${SQL_MASTER_INSTANCE} -Usa -P${SQL_MASTER_SA_PASSWORD} -i "restore-cap-db.sql" -o "restore-cap-db.out" -I -b

echo
echo Bootstrap of the sample database completed successfully.



