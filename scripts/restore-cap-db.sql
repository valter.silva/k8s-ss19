USE master;
GO
-- Enable external scripts execution for R/Python/Java:
exec sp_configure 'external scripts enabled', 1;
RECONFIGURE WITH OVERRIDE;
GO

IF DB_ID('Datawarehouse') IS NULL
        RESTORE DATABASE Datawarehouse
                FROM DISK=N'/var/opt/mssql/backup/DataWarehouse_DPP_Optimised_20181228.bak'
                WITH
                MOVE N'DataWarehouse_Data' TO N'/var/opt/mssql/data/datawarehouse.mdf',
                MOVE N'DataWarehouse_Log' TO N'/var/opt/mssql/data/datawarehouse.ldf';
GO