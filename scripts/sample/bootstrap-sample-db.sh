#!/usr/bin/env bash

CLUSTER_NAMESPACE=$1
SQL_MASTER_IP=$2
SQL_MASTER_SA_PASSWORD=$3
KNOX_IP=$4
KNOX_PASSWORD=$5

# If Knox password is not supplied then default to SQL Master password
KNOX_PASSWORD=${KNOX_PASSWORD:=${SQL_MASTER_SA_PASSWORD}}

SQL_MASTER_INSTANCE=${SQL_MASTER_IP},31433
KNOX_ENDPOINT=${KNOX_IP}:30443

set -x

if [[ ! -f "tpcxbb_1gb.bak" ]]; then
    curl -G "https://sqlchoice.blob.core.windows.net/sqlchoice/static/tpcxbb_1gb.bak" -o tpcxbb_1gb.bak
fi

if [[ ! -f "bootstrap-sample-db.sql" ]]; then
    curl -o bootstrap-sample-db.sql "https://raw.githubusercontent.com/Microsoft/sql-server-samples/master/samples/features/sql-big-data-cluster/bootstrap-sample-db.sql"
fi

kubectl cp tpcxbb_1gb.bak mssql-master-pool-0:/var/opt/mssql/data -c mssql-server -n ${CLUSTER_NAMESPACE}
sqlcmd -S ${SQL_MASTER_INSTANCE} -Usa -P${SQL_MASTER_SA_PASSWORD} -i "bootstrap-sample-db.sql" -o "bootstrap.out" -I -b

for table in web_clickstreams inventory customer
    do
    echo Exporting ${table} data...
    if [[ ${table} == web_clickstreams ]]
    then
        DELIMITER=,
    else
        DELIMITER="|"
    fi
    # WSL ex: "/mnt/c/Program Files/Microsoft SQL Server/Client SDK/ODBC/130/Tools/Binn/bcp.exe"
    bcp sales.dbo.${table} out "$table.csv" -S ${SQL_MASTER_INSTANCE} -Usa -P${SQL_MASTER_SA_PASSWORD} -c -t"$DELIMITER" -e "$table.err"
done


echo Exporting product_reviews data...
bcp "select pr_review_sk, replace(replace(pr_review_content, ',', ';'), char(34), '') as pr_review_content from sales.dbo.product_reviews" queryout "product_reviews.csv" -S ${SQL_MASTER_INSTANCE} -Usa -P${SQL_MASTER_SA_PASSWORD} -c -t, -e "product_reviews.err"

# Copy the data file to HDFS
echo Uploading web_clickstreams data to HDFS...
curl -i -L -k -u root:${KNOX_PASSWORD} -X PUT "https://$KNOX_ENDPOINT/gateway/default/webhdfs/v1/clickstream_data?op=MKDIRS"
curl -i -L -k -u root:${KNOX_PASSWORD} -X PUT "https://$KNOX_ENDPOINT/gateway/default/webhdfs/v1/clickstream_data/web_clickstreams.csv?op=create&overwrite=true" -H 'Content-Type: application/octet-stream' -T "web_clickstreams.csv"
rm -f web_clickstreams.*

echo
echo Uploading product_reviews data to HDFS...
curl -i -L -k -u root:${KNOX_PASSWORD} -X PUT "https://$KNOX_ENDPOINT/gateway/default/webhdfs/v1/product_review_data?op=MKDIRS"
curl -i -L -k -u root:${KNOX_PASSWORD} -X PUT "https://$KNOX_ENDPOINT/gateway/default/webhdfs/v1/product_review_data/product_reviews.csv?op=create&overwrite=true" -H "Content-Type: application/octet-stream" -T "product_reviews.csv"
rm -f product_reviews.*

echo
echo Bootstrap of the sample database completed successfully.



