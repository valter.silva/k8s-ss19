#!/usr/bin/env bash

sudo yum install ntp ntpdate

sudo systemctl start ntpd
sudo systemctl enable ntpd
sudo systemctl status ntpd

sudo sed -i 's,#server 0.centos.pool.ntp.org,server 0.au.pool.ntp.org,g' /etc/ntp.conf
sudo sed -i 's,#server 1.centos.pool.ntp.org,server 1.au.pool.ntp.org,g' /etc/ntp.conf
sudo sed -i 's,#server 2.centos.pool.ntp.org,server 2.au.pool.ntp.org,g' /etc/ntp.conf
sudo sed -i 's,#server 3.centos.pool.ntp.org,server 3.au.pool.ntp.org,g' /etc/ntp.conf

sudo ntpdate -u -s  0.au.pool.ntp.org 1.au.pool.ntp.org 2.au.pool.ntp.org 3.au.pool.ntp.org

sudo systemctl restart ntpd

timedatectl

sudo hwclock  -w

# Add Firewall Rules and Start NTP Daemon
#sudo firewall-cmd --add-service=ntp --permanent
#sudo firewall-cmd --reload

# Manage the service
#sudo systemctl start ntpd
#sudo systemctl enable ntpd
#sudo systemctl status ntpd

#  Verify Server Time Sync
#ntpq -p
#date -R